# Using OpenMP and MPI Parallel Programming on the Wave Project

-----------------------------------

### Abstract

This project analyes the different techniques that can be used on a data sharing application, the wave project. To simulate a drop of water in a grid space, memory needs to be stored within the file for future calculations. To speed up the time it takes for the code to transfer variable data, OpenMP and MPI create ways to separate code to maximize run-time efficiency. In order to test times, clock functions are set up within both programs and the programs are each run/tested 20 separate times. These times are averaged to produce an *average run-time*. The goal is to test this programs run-time capabilties by testing OpenMP methods such as collapse( )/ simd collapse( ) methods, schedule(static/dynamic) method, and reduction( ) method, and MPI methods such as algorithm efficiency, non-blocking communication using MPI_Isend() and MPI_Irecv(), and balancing workload. Results should show what clauses or functions works, why they work in this instance, and how to improve the run-time.

-----------------------------------------

### Installation

You have to download the files from Gitlab to your desired HPCC bash terminal IDE. To run the code you have to be in the correct location for if you want to run OpenMP or the MPI program. To navigate, use `cd` to the correct location, then type the command below to run both programs.


`make sandwich`



-----------------------------------

### Example Code

#### OpenMP Code Example

```c++
int main(int argc, char** argv){

    auto begin = std::chrono::high_resolution_clock::now();
    
        // Domain size 
    double x_min,x_max,y_min,y_max,h,dt,T_final,t;
    int nt;
    int N = 20;
    int M = 20;
    int output_viz = 0;
    
    // Accept input numbers for number of discretization points M & N
    // 
    if (argc > 1)
        M = atoi(argv[1]);

    if (argc > 2)
        N = atoi(argv[2]);
        // Output every output_viz soluiton file
    if (argc > 3)
        output_viz = atoi(argv[3]);

    T_final = 10.0;
    x_min = -1.0;
    x_max = 1.0;
        // Space step
    h = (x_max-x_min)/(M-1);
    double h2i=1.0/h/h;

    y_min = -1.0;
    y_max = y_min + (N-1)*h;

        // timestep;
    dt = 0.5*h/sqrt(2.0);
    cout << "Timestep size: " << dt << endl;
    nt = ceil(T_final/dt) ;
    dt = T_final/nt;
    double dt2=dt*dt;
    cout << "Number of timesteps: " << nt << endl;
    
    Darray1 x,y;
    x.define(1,M);
    y.define(1,N);

        // set up grid
    for (int i = 1; i <=M ; i++)
        x(i) = x_min + h*(i-1);

    for (int j = 1; j <=N ; j++)
        y(j) = y_min + h*(j-1);
    
    Darray2 u,up,um,Lap;
    u.define(1,M,1,N);
    up.define(1,M,1,N);
    um.define(1,M,1,N);
    Lap.define(1,M,1,N);
    
    //#pragma omp simd collapse(2)
    //#pragma omp for schedule(static, 3)
    //#pragma omp parallel for schedule(dynamic,4)
    //#pragma omp parallel for collapse(2) private(it, j)
    for (int j = 1; j <=N ; j++)
        for (int i = 1; i <=M ; i++){
                // Initial data and initialization
            um(i,j) = init_u(x(i),y(j)) - dt*init_dudt(x(i),y(j));
            u(i,j) = init_u(x(i),y(j));
            up(i,j) = 0.0;
            Lap(i,j) = 0.0;
        }
        // Put viz. output at timestep 0 here.

    char fName[100];
    int ifile = 19;
    snprintf(fName, 100, "u_sol_%06d.txt",ifile);
    u.writeToFile(fName,1,M,1,N);
    
    
    snprintf(fName, 100, "x.txt");
    x.writeToFile(fName,1,M);
    snprintf(fName, 100, "y.txt");
    y.writeToFile(fName,1,N); 
    
        // Do some timestepping
    //#pragma omp for schedule(static, 3)
    //#pragma omp parallel for schedule(dynamic, 4)
   // #pragma omp parallel for ordered schedule(dynamic)
   // #pragma omp parallel for collapse(7) private(it, j, i)
    for (int it = 1; it <=nt; it++){
        t = (it-1)*dt;
            // current time
            
            // Compute Laplacian in interior points
        //#pragma omp simd collapse(2)
        for (int j = 2; j <= N-1 ; j++)
            for (int i = 2; i <= M-1 ; i++){
                Lap(i,j) = h2i*(-4*u(i,j)
                                +u(i-1,j)
                                +u(i+1,j)
                                +u(i,j-1)
                                +u(i,j+1));
            }


            // Update time levels 
       // #pragma omp simd collapse(2)
        for (int j = 2; j <= N-1 ; j++)
            for (int i = 2; i <= M-1 ; i++){
                up(i,j) = 2.0*u(i,j)-um(i,j) + dt2*Lap(i,j);
                um(i,j) = u(i,j);
                u(i,j) = up(i,j);
            }
            // Impose boundary conditions

        for (int j = 1; j <=N ; j++)
        {
            u(1,j) = 0.0;
            u(N,j) = 0.0;
        }
        
        
        for (int i = 1; i <=M ; i++){
            u(i,1) = 0.0;
            u(i,M) = 0.0;
        }
            // Put viz. output here.

        if (output_viz > 0){
            if (it%output_viz == 0){
                ifile++;
                snprintf(fName, 100, "u_sol_%06d.txt",ifile);
                u.writeToFile(fName,1,M,1,N);
            }
        }
        
    } // End time loop

    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    cout << "Time to compute [s]: " << elapsed.count()*1e-9 << endl;
    
    return 0;
}
```
#### MPI Code Example

``` c++
for (int it = 1; it <=nt; it++){
            // current time
        t = (it-1)*dt;

            // Set Boundary zero Neumann bondary conditions and 
            // communicate the current u between processes 
            // First BC
        if (rank == 0)
            u(istart-1) = u(istart+1);
        if (rank == size-1)
            u(iend+1) = u(iend-1);

        usl = u(istart);
        
        // TECHNIQUE 2: Non-blocking
        /**MPI_Isend(&usl,1,MPI_DOUBLE,to_my_left,0,MPI_COMM_WORLD,&reqs[2]);
        MPI_Irecv(&urr,1,MPI_DOUBLE,to_my_right,MPI_ANY_TAG,MPI_COMM_WORLD,&reqs[1]);
        
        u(iend+1) = urr;
        usr = u(iend);
        
        
        MPI_Isend(&usr,1,MPI_DOUBLE,to_my_right,1,MPI_COMM_WORLD,&reqs[3]);
        MPI_Irecv(&url,1,MPI_DOUBLE,to_my_left,MPI_ANY_TAG,MPI_COMM_WORLD,&reqs[0]);
        
        
        u(istart-1) = url;
        
        MPI_Waitall(4, reqs, status); 
        **/

        MPI_Sendrecv(&usl,1,MPI_DOUBLE,to_my_left,0,
                     &urr,1,MPI_DOUBLE,to_my_right,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
                     
        u(iend+1) = urr;
               
        usr = u(iend);       
        
        MPI_Sendrecv(&usr,1,MPI_DOUBLE,to_my_right,1,
                     &url,1,MPI_DOUBLE,to_my_left,MPI_ANY_TAG,MPI_COMM_WORLD,&status);
        u(istart-1) = url;
        
       
        MPI_Barrier(MPI_COMM_WORLD);
        
        
        
                // Compute Laplacian in interior points
        for (int i = istart; i <= iend ; i++){
            Lap(i) = h2i*(-2*u(i)+u(i-1)+u(i+1));
            
        }
            // Update time levels 
        for (int i = istart; i <= iend ; i++){
            up(i) = 2.0*u(i)-um(i) + dt2*Lap(i);
            um(i) = u(i);
            u(i) = up(i);
        } 
        
        
```
--------------------------------------

### References

[1]   J. Layton, “Introduction to openmp " admin magazine,” ADMIN Magazine. [Online]. Available: https://www.admin-magazine.com/HPC/Articles/Parallel-Programming-with-OpenMP#:~:text=The%20goal%20is%20to%20keep,is%20exactly%20what%20OpenMP%20does. [Accessed: 30-Apr-2023]. 


[2]   “Cornell Virtual Workshop: Clauses,” cvw.cac.cornell.edu. https://cvw.cac.cornell.edu/openmp/clauses#red (accessed Apr. 30, 2023).


[3]    “Cornell Virtual Workshop: Blocking and non-Blocking,” cvw.cac.cornell.edu. https://cvw.cac.cornell.edu/parallel/block


[4]    “Cornell Virtual Workshop: Overview,” cvw.cac.cornell.edu. https://cvw.cac.cornell.edu/openmp/default (accessed Apr. 30, 2023).


[5]    “Private, firstprivate and lastprivate clauses · OpenMP Little Book,” nanxiao.gitbooks.io. https://nanxiao.gitbooks.io/openmp-little-book/content/posts/private-firstprivate-and-lastprivate-clauses.html (accessed Apr. 30, 2023).


[6]     “Open Multi-Processing (OpenMP) :: High Performance Computing,” hpc.nmsu.edu. https://hpc.nmsu.edu/discovery/mpi/mpi-openmp/ (accessed Apr. 30, 2023).


[7]     “chryswoods.com | Part 1: omp simd features,” chryswoods.com. https://chryswoods.com/vector_c++/features.html (accessed Apr. 30, 2023).


[8]     “What is message passing interface (MPI)? - Definition from WhatIs.com,” SearchEnterpriseDesktop. https://www.techtarget.com/searchenterprisedesktop/definition/message-passing-interface-MPI


[9]     “Chapter 3: schedule,” ppc.cs.aalto.fi. https://ppc.cs.aalto.fi/ch3/schedule/


[10]     “mplot3d tutorial — Matplotlib 2.0.0 documentation,” w.9lo.lublin.pl. http://w.9lo.lublin.pl/DOC/python-matplotlib-doc/html/mpl_toolkits/mplot3d/tutorial.html (accessed Apr. 30, 2023).


[11]      “Blocking and non-blocking MPI communications MPI.” Accessed: Apr. 30, 2023. [Online]. Available: https://web.njit.edu/~shahriar/class_home/HPC/mpi3.pdf


[12]      “MPI Programming -Part 1.” Accessed: Apr. 30, 2023. [Online]. Available: https://www.cs.usask.ca/~spiteri/CMPT851/notes/MPIprogramming.pdf


[13]“NON-BLOCKING COMMUNICATION.” Accessed: Apr. 30, 2023. [Online]. Available: https://www.csc.fi/documents/200270/224572/advanced_MPI.pdf/3a49a631-d403-4bbf-85ba-a8daf17f17d2
