#
# Makefile for Hermite Boundary Conditions 
#

CXX = g++ -std=c++17
LD  = g++
LDFLAGS = -fopenmp
CXXFLAGS = -O0 -DBZ_DEBUG


OBJECTS := main.o 
OBJECTS += Darrays.o 
EXEC = main.x	

.PHONY: clean

exe: $(EXEC)


$(EXEC): $(OBJECTS)
	$(LD) $(LDFLAGS) $(OBJECTS) -o $(EXEC)

%.o : %.cpp
	$(CXX) $(CXXFLAGS) $(CXXINCLUDE) -c $<

%.o : %.f90
	$(FC) $(FCFLAGS) $(FCINCLUDE) -c $<

clean:
	rm -f $(OBJECTS) $(EXEC) 

sandwich: 
	make clean
	make 
	./main.x

